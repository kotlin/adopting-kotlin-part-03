package j;

public class LongPerformance {

    public static void main(String[] args) {
        System.out.println("Java test");
        useObject();
        usePrimitive();
    }

    public static void useObject() {
        Long sum = 0L;
        long start = System.currentTimeMillis();
        for (long i = 0; i <= Integer.MAX_VALUE; i++) {
            sum = sum + i;
        }
        long stop = System.currentTimeMillis();
        System.out.println("Sum: [" + sum + "], time: [" + (stop - start) + "] ms");
    }

    public static void usePrimitive() {
        long sum = 0L;
        long start = System.currentTimeMillis();
        for (long i = 0; i <= Integer.MAX_VALUE; i++) {
            sum += i;
        }
        long stop = System.currentTimeMillis();
        System.out.println(sum);
        System.out.println("Sum: [" + sum + "], time: [" + (stop - start) + "] ms");
    }
}