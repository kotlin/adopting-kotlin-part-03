package k

fun main(args: Array<String>) {

    println("Kotlin test")
    var sum: Long = 0L
    val start = System.currentTimeMillis()
    for (i in 0..Integer.MAX_VALUE) {
        sum = sum + i
    }
    val stop = System.currentTimeMillis()
    println("Sum: [" + sum + "], time: [" + (stop - start) +"] ms")

}